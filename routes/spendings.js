const express = require('express');
const router = express.Router();

const spendingsController = require('../controllers/spendingsController');

router.route('/')
  .get(spendingsController.getByDay)
  .post(spendingsController.createSpending);

router.route('/:spendingId')
  .get(spendingsController.getById)
  .delete(spendingsController.deleteById)
  .post(spendingsController.updateSpending);

module.exports = router;