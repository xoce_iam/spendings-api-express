const db = require('./dbconnect');

console.log('SPENDINGS MODEL');

Spendings = {
  // get spending by id
  getById: function(req, res) {        
    db.query('SELECT * FROM expenses WHERE id = ?', req.params.spendingId, function(err, rows) {
      if (err) { 
        res.json({
          status: 'error', 
          'message': 'Mysql Error'
        });
      }

      var apiResult = {};

      apiResult = {
        status: 'success',
        rows: rows,
      }

      res.json(apiResult);
    });
  },

  //get spending by date 
  getByDay: function(req, res) {
    var date;
    
    // check if query string date is present. use current date if not
    if(req.query.date) {
        date = req.query.date;
    } else {
        date = req.currentDate;
    }
    
    db.query('SELECT * FROM expenses WHERE date = ? AND user_id = ?', [date, req.currentUser.id], function(err, rows) {
        if (err) { 
          res.json({
            status: 'error', 
            'message': 'Mysql Error'
          });
        }

        var apiResult = {}; 
        var totalAmount = 0;

        // get total amount of error
        rows.forEach(function(row, index, arr) {
          totalAmount += row.amount;
        });

        apiResult = {
          status: 'success',
          rows: rows,
          totalAmount: totalAmount,
        }

        res.json(apiResult);
    });
  },

  createSpending: function(req, res) {
    var spending = {
      user_id: req.currentUser.id,
      name:req.body.name,
      description:req.body.description,
      amount:req.body.amount,
      date:req.body.date,
    };

    db.query('INSERT INTO expenses SET ?', spending, function(err, result) {
      if (err) { 
        res.json({
          status: 'error', 
          'message': 'Mysql Error'
        });
      }

      res.json({
        status: 'success', 
        message: 'Spending has been saved'
      });
    });
  },

  deleteById: function(req, res) {
    db.query('DELETE FROM expenses WHERE id = ? and user_id = ?', [req.params.spendingId, req.currentUser.id], function(err, result) {
      if (err) { 
        res.json({
          status: 'error', 
          'message': 'Mysql Error'
        });
      }

      res.json({
        status: 'success', 
        message: 'Spending has been deleted'
      });
    })
  },

  updateSpending: function(req, res) {
    var spending = {
      name:req.body.name,
      description:req.body.description,
      amount:req.body.amount,
      date:req.body.date,
    };

    db.query('UPDATE expenses SET ? WHERE id = ? AND user_id = ?', [spending, req.params.spendingId, req.currentUser.id], function(err, result) {
      if (err) { 
        res.json({
          status: 'error', 
          'message': 'Mysql Error'
        });
      }

      res.json({
        status: 'success', 
        message: 'Spending has been updated'
      });
    });
  }
}

module.exports = Spendings;