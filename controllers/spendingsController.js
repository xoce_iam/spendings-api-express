const spendingsModel = require('../models/spendingsModel');

console.log("SPENDINGS CONTROLLER");

var SpendingsController = {
  getByDay: function(req, res) {
    spendingsModel.getByDay(req, res);
  },

  getById: function(req, res) {
    spendingsModel.getById(req, res);
  },

  createSpending: function(req, res) {
    //TODO: add validation
    spendingsModel.createSpending(req, res);
  },

  deleteById: function(req, res) {
    spendingsModel.deleteById(req, res);
  },

  updateSpending: function(req, res) {
    //TODO: add validation
    spendingsModel.updateSpending(req, res);
  },
}

module.exports = SpendingsController;