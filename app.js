const express = require('express');
const bodyParser = require('body-parser');

const spendingRoutes = require('./routes/spendings');

app = express();

app.use(bodyParser.urlencoded({ extended: false}));


//get current date
app.use(function (req, res, next) {
  var date = new Date();

  var formattedDate = ('0' + date.getDate()).slice(-2);
  var formattedMonth = ('0' + (date.getMonth() + 1)).slice(-2);
  var formattedYear = date.getFullYear().toString();

  req.currentDate = formattedYear + '-' + formattedMonth + '-' + formattedDate ;
  next();
});

//set user profile
app.use(function (req, res, next) {
  var token = req.query.token;

  //TODO: get user profile by token
  var user = {
    'id': 1,
    'name': 'X'
  }

  req.currentUser = user;
  next();
})


app.use('/spendings', spendingRoutes);

app.get('/', function(req, res) {
  console.log('landing');
  res.send('API LANDING');
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

app.use((err, req, res, next) => {
  res.locals.error = err;
  res.status(err.status);
  res.json({status: err.status, 'message': err.message});
});


app.listen(3000, function() {
	console.log('The application is running on localhost:3000!')
});